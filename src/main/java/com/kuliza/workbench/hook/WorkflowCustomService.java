package com.kuliza.workbench.hook;

import com.kuliza.lending.journey.base.WorkflowDefaultService;
import com.kuliza.lending.journey.models.WorkFlowApplication;
import com.kuliza.lending.journey.models.WorkFlowSource;
import com.kuliza.lending.journey.models.WorkFlowUserApplication;
import com.kuliza.lending.journey.pojo.WorkflowInitiateRequest;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service("workflowCustomService")
public class WorkflowCustomService extends WorkflowDefaultService {

  @Override
  public void preInitiateScript(
      String username,
      WorkflowInitiateRequest workflowInitiateRequest,
      WorkFlowApplication wfAppObj,
      WorkFlowUserApplication wfUserAppObj,
      Map<String, Object> processVariables,
      WorkFlowSource wfSourceObj) {
    // Any custom code before initiating the process should come here.
  }
}
