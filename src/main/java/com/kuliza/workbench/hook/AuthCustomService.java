package com.kuliza.workbench.hook;

import com.kuliza.lending.common.pojo.AbstractResponse;
import com.kuliza.lending.common.pojo.ApiResponseFactory;
import com.kuliza.lending.journey.base.AuthDefaultService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("authCustomService")
public class AuthCustomService extends AuthDefaultService {

  @Override
  public AbstractResponse sendUserPassword(String userIdentifier, String otp) {
    // Logic to send Password to the user.
    return ApiResponseFactory.constructApiSuccessResponse(HttpStatus.OK);
  }
}
