package com.kuliza.workbench.hook;

import com.kuliza.lending.common.exception.BadRequestException;
import com.kuliza.lending.engine_common.base.DMSDefaultService;
import com.kuliza.lending.engine_common.pojo.FileUploadRequest;
import com.kuliza.lending.engine_common.pojo.ProductDMSUploadResponse;
import org.springframework.stereotype.Service;

@Service("dmsCustomService")
public class DMSCustomService extends DMSDefaultService {

  @Override
  public ProductDMSUploadResponse uploadFile(String userId, FileUploadRequest uploadForm)
      throws BadRequestException {
    // Custom code to download the file should come here.
    return null;
  }
}
